"use strict";
(function() {
    const cookiesDisabled = !navigator.cookieEnabled;
    if (cookiesDisabled) return console.warn("WARNING: Pager disabled due to cookie restrictions");
    let seek;
    let pager = {};
    const key = "config.scroll.pager.urls";
    if (!localStorage[key]) localStorage[key] = JSON.stringify(pager);
    let url = function() {
        return self.location.href.split("#")[0].split("?")[0];
    };
    const scrollHash = function(url) {
        const hash = self.location.hash;
        const fragment = hash.slice(1) && document.getElementById(hash.slice(1));
        const fragmented = hash.length > 0;
        const hashed = fragmented && document.body.contains(fragment);
        if (hashed) {
            self.location.hash = hash;
            self.location.href = hash;
            if ("scroll-margin-top" in document.body.style === false && fragment.textContent !== "") {
                self.scrollBy(0, -6 * parseFloat(getComputedStyle(document.documentElement).fontSize));
            }
        }
        return hashed;
    };
    const scrollRestore = function(url) {
        if (history.scrollRestoration) history.scrollRestoration = "manual";
        if (scrollHash(url)) return;
        pager = JSON.parse(localStorage[key]);
        if (pager[url] > 0) {
            clearInterval(seek);
            self.scrollTo(0, pager[url]);
            let i = 0;
            return seek = setInterval(function(position) {
                i++;
                if (i > 100) clearInterval(seek);
                if (document.documentElement.scrollHeight >= position + document.documentElement.clientHeight) {
                    clearInterval(seek);
                    self.scrollTo(0, position);
                }
            }, 4, pager[url]);
        } else self.scrollTo(0, 0);
        pager[url] = self.pageYOffset;
        localStorage[key] = JSON.stringify(pager);
    };
    const scrollTrack = function(url) {
        const currentPosition = self.pageYOffset;
        pager = JSON.parse(localStorage[key]);
        pager[url] = currentPosition;
        localStorage[key] = JSON.stringify(pager);
    };
    const scrollReverse = function(back, up, event) {
        if (document.body.contains(up) && up.contains(event.target)) {
            event.preventDefault();
            window.scrollTo(0, 0);
        }
        if (document.body.contains(back) && back.contains(event.target)) {
            if (history.length < 2) return;
            event.preventDefault();
            history.go(-1);
        }
    };
    [
        "DOMContentLoaded",
        "pageshow",
        "hashchange",
        "URLChangedCustomEvent"
    ].forEach(function(event) {
        self.addEventListener(event, function(event) {
            if (event.type === "pageshow") {
                return event.persisted && self.scrollTo(0, pager[url()]);
            }
            if (event.type === "DOMContentLoaded") {
                self.addEventListener("click", function(event) {
                    const up = document.getElementById("top");
                    const back = document.getElementById("back");
                    scrollReverse(back, up, event);
                });
            }
            scrollRestore(url());
        });
    });
    [
        "click",
        "touchstart",
        "scroll"
    ].forEach(function(event) {
        self.addEventListener(event, function() {
            scrollTrack(url());
        });
    });
})();
(function() {
    const cookiesDisabled = !navigator.cookieEnabled;
    if (cookiesDisabled) return console.warn("WARNING: Update check disabled due to cookie restrictions");
    function fetch(url, method, callback) {
        const http = new XMLHttpRequest();
        http.onreadystatechange = function() {
            if (http.readyState === 4 && http.status === 200) {
                if (callback) callback(http);
            }
        };
        http.open(method, url);
        http.setRequestHeader("Pragma", "no-cache");
        http.setRequestHeader("Cache-Control", "no-cache");
        http.send();
        return http;
    }
    const key = "config.update";
    let stamps = {};
    if (!sessionStorage[key + ".urls"]) sessionStorage[key + ".urls"] = JSON.stringify(stamps);
    function update() {
        const url = self.location.href.split("#")[0].split("?")[0];
        const indicator = document.querySelector("a[data-update]");
        if (indicator === null || indicator.dataset.update === "refresh") return;
        const anchor = indicator.cloneNode();
        fetch(url, "HEAD", function(request) {
            const local = document.querySelector('meta[name="last-modified"]').content || document.lastModified;
            const remote = request.getResponseHeader("last-modified") || '';
            const modified = Date.parse(remote || local) > Date.parse(local);
            const drift = Date.parse(remote || local) - Date.parse(local);
            if (drift < 10000) return;
            function reset() {
                indicator.href = anchor.href;
                indicator.dataset.update = anchor.dataset.update;
            }
            stamps = JSON.parse(sessionStorage[key + ".urls"]);
            if (stamps[url] === remote) return;
            stamps[url] = remote;
            sessionStorage[key + ".urls"] = JSON.stringify(stamps);
            if (remote && modified) {
                fetch(url, "GET", function() {
                    indicator.href = url.replace(/^https:/, "http:");
                    indicator.removeAttribute("id");
                    indicator.dataset.update = "refresh";
                    indicator.addEventListener("click", function(event) {
                        event.preventDefault();
                        reset();
                        self.location.reload();
                    });
                    console.log("INFO: R: " + remote);
                    console.log("INFO: L: " + local);
                    console.log("INFO: D: " + drift);
                    console.log("INFO: M: " + modified);
                });
            }
        });
    }
    let scrolled;
    let delay = 1000;
    let delayed = 0;
    self.addEventListener("scroll", function() {
        if (scrolled) clearTimeout(scrolled);
        scrolled = setTimeout(function() {
            update();
            delay = delay + delayed;
            delayed = delay - delayed;
        }, delay);
    });
    [
        "focus",
        "load",
        "URLChangedCustomEvent"
    ].forEach(function(event) {
        self.addEventListener(event, function() {
            update();
        });
    });
})();
(function() {
    const options = "targetWindow,width=700,height=500,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes";
    self.addEventListener("keydown", function(event) {
        if (event.repeat && event.key === "d") {
            selection(dictionary);
        }
        if (event.repeat && event.key === "s") {
            selection(search);
        }
        if (event.repeat && event.key === "m") {
            selection(manual);
        }
    });
    function selection(execute) {
        let phrase = "" + window.getSelection();
        phrase = phrase.replace(/[!.:?,;"]/g, "");
        phrase = phrase.replace(/^\s*(\S*?)\s*$/g, "$1");
        if (phrase && phrase > "" && phrase.length > 1) {
            execute(phrase);
        }
    }
    function dictionary(word) {
        window.open("https://www.merriam-webster.com/dictionary/" + encodeURIComponent(word), "Definitions", options);
    }
    function search(phrase) {
        window.open("https://lite.duckduckgo.com/lite/?q=" + encodeURIComponent(phrase), "Search", options);
    }
    function manual(program) {
        window.open("https://man.archlinux.org/search?q=" + encodeURIComponent(program), "Manual", options);
    }
})();
(function() {
    if (typeof Element !== "undefined") {
        if (!Element.prototype.matches) {
            Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
        }
        if (!Element.prototype.closest) {
            Element.prototype.closest = function(s) {
                var el = this;
                do {
                    if (el.matches(s)) return el;
                    el = el.parentElement || el.parentNode;
                }while (el !== null && el.nodeType === 1)
                return null;
            };
        }
    }
    function fetch(url, method, callback) {
        const http = new XMLHttpRequest();
        http.onreadystatechange = function() {
            if (http.readyState === 4 && http.status === 200) {
                if (callback) callback(http);
            }
        };
        http.open(method, url);
        http.send();
        return http;
    }
    self.addEventListener("DOMContentLoaded", function() {
        [
            "mouseout",
            "mousedown",
            "touchstart"
        ].forEach(function(event) {
            self.addEventListener(event, function(event) {
                const url = event.target.closest("a");
                if (preloadable(url) === undefined) return;
                preload(url.href);
            });
        });
        function preloadable(url) {
            switch(true){
                case url === null || url.href === null:
                    return;
                case url.origin !== location.origin:
                    return;
                case [
                    "http:",
                    "https:"
                ].includes(url.protocol) === null:
                    return;
                case url.protocol === "http:" && location.protocol === "https:":
                    return;
                case url.hash && url.pathname + url.search == location.pathname + location.search:
                    return;
                default:
                    return true;
            }
        }
        function preload(url) {
            const prefetcher = document.createElement("link");
            prefetcher.rel = "custom-prefetch";
            prefetcher.href = url;
            const selector = 'link[rel="'.concat(prefetcher.rel, '"][href="').concat(prefetcher.href, '"]');
            const prefetched = document.head.contains(document.querySelector(selector));
            if (prefetched) {
                return;
            }
            document.head.appendChild(prefetcher);
            fetch(url, "GET", function() {});
        }
    });
})();
(function() {
    const hide = function(triggers) {
        for(let i = 0; i < triggers.length; i++){
            triggers[i].checked = false;
        }
    };
    const hideIfClickedOutside = function(menus, triggers, event) {
        for(let i = 0; i < menus.length; i++){
            const active = triggers[i].checked === true;
            const outside = !menus[i].contains(event.target);
            if (outside && active) hide(triggers);
        }
    };
    self.addEventListener("scroll", function() {
        const triggers = document.querySelectorAll("menu input");
        hide(triggers);
    });
    [
        "click",
        "touchstart"
    ].forEach(function(event) {
        self.addEventListener(event, function(event) {
            const menus = document.querySelectorAll("menu");
            const triggers = document.querySelectorAll("menu input");
            hideIfClickedOutside(menus, triggers, event);
        });
    });
})();
(function() {
    [
        "DOMContentLoaded",
        "URLChangedCustomEvent"
    ].forEach(function(event) {
        self.addEventListener(event, function() {
            const Container = function() {
                return document.querySelector("search-box");
            };
            const container = Container();
            const Dropdown = function() {
                return document.querySelector("search-box ul");
            };
            const dropdown = Dropdown();
            const Form = function() {
                return document.querySelector("search-box form");
            };
            const form = Form();
            const Query = function() {
                return document.querySelector("search-box input");
            };
            const query = Query();
            const Submit = function() {
                return document.querySelector("search-box button");
            };
            const submit = Submit();
            function first(element) {
                if (element) return element.firstElementChild;
            }
            function last(element) {
                if (element) return element.lastElementChild;
            }
            function next(element) {
                return element.parentElement.nextElementSibling.querySelector("a").focus();
            }
            function previous(element) {
                return element.parentElement.previousElementSibling.querySelector("a").focus();
            }
            function press(keyname) {
                document.dispatchEvent(new KeyboardEvent("keydown", {
                    "key": keyname
                }));
            }
            const Action = {
                cancel: function() {
                    Dropdown().setAttribute("hidden", "");
                    Container().removeAttribute("data-focus");
                },
                reveal: function() {
                    Dropdown().removeAttribute("hidden");
                    Container().setAttribute("data-focus", "");
                }
            };
            if (submit === null) return;
            let selected;
            submit.addEventListener("click", function(event) {
                press("ArrowDown");
                container.setAttribute("data-focus", "");
                initialize();
                if (selected) {
                    event.preventDefault();
                    selected.focus();
                    return selected.click();
                }
                first(dropdown).querySelector("a").focus();
                document.activeElement.click();
            });
            [
                "keyup",
                "click"
            ].forEach(function(event) {
                form.addEventListener(event, function() {
                    if (document.activeElement.nodeName === "A") return selected = document.activeElement;
                    selected = undefined;
                });
            });
            form.addEventListener("focusin", function() {
                container.setAttribute("data-focus", "");
                initialize();
            });
            form.addEventListener("submit", function(event) {
                event.preventDefault();
                return false;
            });
            form.addEventListener("keydown", function(event) {
                if (event.keyCode == 40) {
                    event.preventDefault();
                    if (document.activeElement == query) first(dropdown).querySelector("a").focus();
                    else if (document.activeElement == last(dropdown).querySelector("a")) query.focus();
                    else next(document.activeElement);
                    return;
                }
                if (event.keyCode == 38) {
                    event.preventDefault();
                    if (document.activeElement == query) last(dropdown).querySelector("a").focus();
                    else if (document.activeElement == first(dropdown).querySelector("a")) query.focus();
                    else previous(document.activeElement);
                    return;
                }
                if (event.keyCode == 13) {
                    if (dropdown && document.activeElement == query) {
                        event.preventDefault();
                        first(dropdown).querySelector("a").focus();
                        first(dropdown).querySelector("a").click();
                    }
                    return;
                }
                if (form.contains(event.target)) {
                    if (event.keyCode == 27) {
                        event.preventDefault();
                        document.activeElement.blur();
                        return Action.cancel();
                    }
                }
                if (document.activeElement != query) {
                    query.focus();
                }
            });
            if (event === "DOMContentLoaded") {
                let scrolls = 0;
                self.addEventListener("scroll", function() {
                    if (scrolls > 75) {
                        scrolls = 0;
                        document.activeElement.blur();
                        Action.cancel();
                    }
                    scrolls++;
                });
                self.addEventListener("click", function(event) {
                    if (!Form().contains(event.target) && !(document.activeElement === Query()) && !(document.activeElement === Submit())) {
                        Action.cancel();
                    }
                });
            }
            function fetch(url, callback) {
                const http = new XMLHttpRequest();
                http.onreadystatechange = function() {
                    if (http.readyState === 4 && http.status === 200 && callback) callback(http);
                };
                http.open("GET", url);
                http.send();
            }
            function script(url) {
                return new Promise(function(resolve, reject) {
                    const script = document.createElement("script");
                    script.onerror = reject;
                    script.onload = resolve;
                    if (document.currentScript) document.currentScript.parentNode.insertBefore(script, document.currentScript);
                    else document.head.appendChild(script);
                    script.src = url;
                });
            }
            let data = {};
            let boot = true;
            let origin = document.querySelector("meta[name='base-url']");
            const options = {
                key: [
                    "title"
                ]
            };
            function isEmpty(obj) {
                return Object.keys(obj).length === 0;
            }
            function appendItemsTo(local, remote) {
                const paginated = Object.keys(remote).includes("next_url");
                if (isEmpty(local)) local = remote;
                else local.items = local.items.concat(remote.items);
                if (paginated) fetch(remote.next_url, function(request) {
                    appendItemsTo(local, JSON.parse(request.responseText));
                });
                else search(query.value, data.items, options);
                data = local;
            }
            function initialize() {
                if (boot) {
                    boot = false;
                    if (origin) origin = origin.content.replace(/\/+$/, "");
                    else origin = window.location.origin;
                    script(origin + "/js/fuzzysort.js").then(function() {
                        fetch(origin + "/index.json", function(request) {
                            appendItemsTo({}, JSON.parse(request.responseText));
                            Action.reveal();
                            search("", data.items, options);
                        });
                        [
                            "keyup",
                            "focusin"
                        ].forEach(function(event) {
                            query.addEventListener(event, function() {
                                if (data.items) search(query.value, data.items, options);
                                else {
                                    boot = true;
                                    initialize();
                                }
                            });
                        });
                    }).catch(function(error) {
                        console.error("ERROR: Failed to load fuzzy search", error);
                    });
                } else Action.reveal();
            }
            function escape(text) {
                const escaped = document.createElement("textarea");
                escaped.textContent = text;
                return escaped.innerHTML;
            }
            function search(term, data, options) {
                if (typeof fuzzysort !== "object") return;
                let items = "";
                const results = fuzzysort.go(term, data, options);
                if (results.length === 0 && term.length >= 0) {
                    let separator = "—";
                    if (term.length === 0) separator = "";
                    const li = document.createElement("li");
                    const a = document.createElement("a");
                    a.textContent = `${term} ${separator} No Results Found`;
                    li.appendChild(a);
                    items = li.outerHTML;
                } else {
                    for(var string in results.slice(0, 10)){
                        const title = results[string].obj.title;
                        const href = results[string].obj.url;
                        let highlight = fuzzysort.highlight(fuzzysort.single(escape(term), escape(title)), "<span>", "</span>");
                        if (highlight === null) highlight = title;
                        const li = document.createElement("li");
                        const a = document.createElement("a");
                        a.href = href;
                        a.tabIndex = 0;
                        a.innerHTML = highlight;
                        li.appendChild(a);
                        items = items + li.outerHTML;
                    }
                }
                if (dropdown.innerHTML !== items) dropdown.innerHTML = items;
            }
        });
    });
})();
(function() {
    const cookiesDisabled = !navigator.cookieEnabled;
    if (cookiesDisabled) return console.warn("WARNING: Autoplay control disabled due to cookie restrictions");
    function viewport(element) {
        const options = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : {
            offset: {
                top: -50,
                left: 0,
                bottom: -50,
                right: 0
            }
        };
        const view = element.getBoundingClientRect();
        return view.top >= -options.offset.top && view.left >= -options.offset.left && view.bottom <= (self.innerHeight || document.documentElement.clientHeight) + options.offset.bottom && view.right <= (self.innerWidth || document.documentElement.clientWidth) + options.offset.right;
    }
    const state = "on";
    const key = "config.video.autoplay";
    if (!localStorage[key]) localStorage[key] = state;
    [
        "scroll",
        "DOMContentLoaded",
        "pointerover",
        "touchstart",
        "touchend"
    ].forEach(function(event) {
        self.addEventListener(event, function(event) {
            if (event.type === "pointerover" || event.type === "touchstart" || event.type === "touchend") {
                if (event.target.nodeName === "VIDEO") {
                    event.target.setAttribute("data-autoplay", false);
                }
                return;
            }
            let first = true;
            const videos = document.querySelectorAll("video");
            for(let i = 0; i < videos.length; i++){
                videos[i].autoplay = true;
                videos[i].controls = true;
                videos[i].loop = true;
                videos[i].muted = true;
                videos[i].playsinline = true;
                videos[i].setAttribute("autoplay", true);
                videos[i].setAttribute("controls", true);
                videos[i].setAttribute("loop", true);
                videos[i].setAttribute("muted", true);
                videos[i].setAttribute("playsinline", true);
                const onscreen = viewport(videos[i]);
                const uninterrupted = !videos[i].dataset.autoplay;
                if (first && onscreen && uninterrupted && localStorage[key] === state) {
                    videos[i].play();
                    first = false;
                } else {
                    videos[i].pause();
                }
            }
        });
    });
})();
(function() {
    if (typeof Element !== "undefined") {
        if (!Element.prototype.matches) {
            Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
        }
        if (!Element.prototype.closest) {
            Element.prototype.closest = function(s) {
                var el = this;
                do {
                    if (el.matches(s)) return el;
                    el = el.parentElement || el.parentNode;
                }while (el !== null && el.nodeType === 1)
                return null;
            };
        }
    }
    const disabled = "0s";
    function walk(children, callback) {
        for(let i = 0; i < children.length; i++){
            callback(children[i]);
            walk(children[i].children, callback);
        }
    }
    self.addEventListener("mousemove", function(event) {
        if (typeof event.target.closest !== "function") return;
        const tree = event.target.closest("figure") || event.target.closest("article");
        if (tree !== null) {
            walk(tree.children, function(element) {
                const delay = self.getComputedStyle(element).getPropertyValue("transition-delay");
                if (delay !== disabled) {
                    element.style.setProperty("visibility", "hidden");
                }
            });
            walk(tree.children, function(element) {
                const delay = self.getComputedStyle(element).getPropertyValue("transition-delay");
                if (delay !== disabled) {
                    element.style.removeProperty("visibility");
                }
            });
        }
    });
})();
(function() {
    const cookiesDisabled = !navigator.cookieEnabled;
    if (cookiesDisabled) return console.warn("WARNING: Form state cannot persist due to cookie restrictions");
    const storage = document.createEvent("Event");
    storage.initEvent("storage", true, true);
    [
        "pageshow",
        "FormsCustomEvent",
        "URLChangedCustomEvent",
        "DOMContentLoaded"
    ].forEach(function(event) {
        self.addEventListener(event, function(event) {
            const input = Array.prototype.slice.call(document.querySelectorAll("form input"));
            const select = Array.prototype.slice.call(document.querySelectorAll("form select"));
            const textarea = Array.prototype.slice.call(document.querySelectorAll("form textarea"));
            const summary = Array.prototype.slice.call(document.querySelectorAll("details summary"));
            const states = input.concat(select).concat(textarea);
            for(var i = 0; i < states.length; i++){
                const state = states[i];
                if (localStorage[state.id]) {
                    if (state.type === "radio" || state.type === "checkbox") {
                        if (localStorage[state.id] === "on") state.checked = true;
                    } else state.value = localStorage[state.id];
                }
                if (state.dataset.listener === "active") break;
                state.setAttribute("data-listener", "active");
                state.addEventListener("change", function(event) {
                    localStorage[event.target.id] = event.target.value;
                    const group = document.querySelectorAll("input[name='".concat(event.target.name, "']"));
                    for(var j = 0; j < group.length; j++){
                        const member = group[j];
                        if ((member.type === "radio" || member.type === "checkbox") && !member.checked) {
                            localStorage[member.id] = "off";
                        }
                    }
                    self.dispatchEvent(storage);
                });
            }
            for(var k = 0; k < summary.length; k++){
                let child = summary[k];
                let details = child.parentElement;
                if (details.id && details.nodeName === "DETAILS") {
                    sessionStorage[details.id] === "false" && details.removeAttribute("open");
                    sessionStorage[details.id] === "true" && details.setAttribute("open", true);
                    child.addEventListener("click", function(event) {
                        let child = event.target.nodeName === "SUMMARY" && event.target || event.target.parentElement;
                        let details = child.parentElement;
                        if (details.id && details.nodeName === "DETAILS") {
                            sessionStorage[details.id] = !details.open;
                        }
                    });
                }
            }
        });
    });
    [
        "storage"
    ].forEach(function(event) {
        self.addEventListener(event, function() {
            let stylesheet;
            stylesheet = document.querySelector('link[href$="default-simple.css"]');
            if (localStorage["config.layout.simple"] === "on") stylesheet.rel = "stylesheet";
            if (localStorage["config.layout.default"] === "on") stylesheet.rel = "alternate stylesheet";
            stylesheet = document.querySelector('link[href$="default-fast.css"]');
            if (localStorage["config.navigation.fast"] === "on") stylesheet.rel = "stylesheet";
            if (localStorage["config.navigation.slow"] === "on") stylesheet.rel = "alternate stylesheet";
            for(var i = 0; i < document.styleSheets.length; i++){
                let stylesheet = document.styleSheets[i];
                if (!stylesheet.href || !stylesheet.href.startsWith(self.origin)) continue;
                for(var k = 0; k < stylesheet.rules.length; k++){
                    let media = stylesheet.rules[k].media;
                    if (media === undefined || !media.mediaText.startsWith("(prefers-color-scheme:")) continue;
                    switch(true){
                        case localStorage["config.theme.light"] === "on":
                            media.mediaText = "(prefers-color-scheme: dark)";
                            if (getComputedStyle(document.body).getPropertyValue("color-scheme") === "dark") media.mediaText = "(prefers-color-scheme: light)";
                            continue;
                        case localStorage["config.theme.auto"] === "on":
                            media.mediaText = "(prefers-color-scheme: dark)";
                            continue;
                        case localStorage["config.theme.dark"] === "on":
                            media.mediaText = "(prefers-color-scheme: light)";
                            if (getComputedStyle(document.body).getPropertyValue("color-scheme") === "light") media.mediaText = "(prefers-color-scheme: dark)";
                    }
                }
            }
        });
    });
    const early = setInterval(persistence, 4);
    function persistence() {
        if (document.styleSheets.length > 0) {
            self.dispatchEvent(storage);
            clearInterval(early);
        }
    }
    self.addEventListener("DOMContentLoaded", function() {
        self.dispatchEvent(storage);
        clearInterval(early);
    });
})();
(function() {
    const cookiesDisabled = !navigator.cookieEnabled;
    if (cookiesDisabled) return console.warn("WARNING: DOM filter disabled due to cookie restrictions");
    const timeout = 300;
    const state = "off";
    const key = "config.navigation.fast";
    if (!localStorage[key]) localStorage[key] = state;
    function fetch(url, method, callback) {
        const http = new XMLHttpRequest();
        http.onreadystatechange = function() {
            if (callback && http.readyState === 4) {
                if (http.status === 200) callback(http);
                else {
                    console.error("ERROR: Unable to navigate", http);
                    self.location.href = url;
                }
            }
        };
        http.open(method, url);
        http.timeout = timeout;
        http.send();
        return http;
    }
    function styles(node) {
        return node.nodeName === "LINK" && node.rel && node.rel.includes("stylesheet");
    }
    function scripts(node) {
        return node.nodeName === "SCRIPT" && node.hasAttribute("src");
    }
    function filter(url, http) {
        let live = document;
        let node = live.head.childNodes.length;
        let next = new DOMParser().parseFromString(http.responseText, "text/html");
        if (next.doctype === null || !http.getResponseHeader("content-type").includes("text/html")) return false;
        while(node--){
            if (styles(live.head.childNodes[node]) || scripts(live.head.childNodes[node])) {} else {
                live.head.removeChild(live.head.childNodes[node]);
            }
        }
        while(next.head.firstChild){
            if (styles(next.head.firstChild) || scripts(next.head.firstChild)) {
                next.head.removeChild(next.head.firstChild);
            } else {
                live.head.appendChild(next.head.firstChild);
            }
        }
        while(live.documentElement.attributes.length > 0){
            live.documentElement.removeAttribute(live.documentElement.attributes[live.documentElement.attributes.length - 1].name);
        }
        while(next.documentElement.attributes.length > 0){
            live.documentElement.setAttribute(next.documentElement.attributes[next.documentElement.attributes.length - 1].name, next.documentElement.attributes[next.documentElement.attributes.length - 1].value);
            next.documentElement.removeAttribute(next.documentElement.attributes[next.documentElement.attributes.length - 1].name);
        }
        live.body.parentElement.replaceChild(next.body, live.body);
    }
    function persist() {
        let persist = document.createElement("link");
        persist.rel = "location";
        persist.target = JSON.stringify(self.location);
        document.head.appendChild(persist);
    }
    self.addEventListener("DOMContentLoaded", function() {
        if (!localStorage[key] || localStorage[key] === state) return;
        persist();
    });
    self.addEventListener("popstate", function(event) {
        if (!localStorage[key] || localStorage[key] === state) return;
        persist();
        const link = JSON.parse(document.querySelector('link[rel="location"]').target);
        const url = event.target.location;
        const hashed = link.pathname === url.pathname;
        if (hashed) return;
        fetch(url, "GET", function(http) {
            if (filter(url, http) === false) return self.location.href = url;
            persist();
            self.document.dispatchEvent(new CustomEvent("URLChangedCustomEvent", {
                bubbles: true
            }));
        });
    });
    self.addEventListener("click", function(event) {
        if (!localStorage[key] || localStorage[key] === state) return;
        const links = document.querySelectorAll("a");
        for(let i = 0; i < links.length; i++){
            const active = links[i].contains(event.target);
            const change = self.location.href !== links[i].href;
            const view = links[i].attributes.hasOwnProperty("download") === false;
            const local = self.location.origin === links[i].origin && links[i].target !== "_self";
            const hashed = self.location.pathname === links[i].pathname && links[i].href.includes("#");
            if (active && local && change && view && hashed === false) {
                event.preventDefault();
                const url = links[i].href;
                links[i].style.cursor = "wait";
                fetch(url, "GET", function(http) {
                    links[i].removeAttribute("style");
                    if (filter(url, http) === false) return self.location.href = url;
                    history.pushState({}, "", links[i].href);
                    persist();
                    self.document.dispatchEvent(new CustomEvent("URLChangedCustomEvent", {
                        bubbles: true
                    }));
                });
            }
        }
    });
})();
(function() {
    const cookiesDisabled = !navigator.cookieEnabled;
    if (cookiesDisabled) return console.warn("WARNING: Native pagination fallback due to cookie restrictions");
    function fetch(url, method, callback, fallback) {
        const http = new XMLHttpRequest();
        http.onreadystatechange = function() {
            if (callback && http.readyState === 4) {
                if (http.status === 200) callback(http);
                else fallback(http);
            }
        };
        http.open(method, url);
        http.send();
        return http;
    }
    let abort = false;
    const state = "off";
    const key = "config.scroll.infinite";
    if (!localStorage[key]) localStorage[key] = state;
    [
        "scroll",
        "DOMContentLoaded"
    ].forEach(function(event) {
        self.addEventListener(event, function() {
            if (abort || !localStorage[key] || localStorage[key] === state) return;
            const remaining = Math.abs(document.documentElement.scrollHeight - document.documentElement.clientHeight - self.pageYOffset);
            const traversed = self.pageYOffset;
            const journey = remaining + traversed;
            const ratio = traversed / journey * 100;
            const threshold = ratio >= 50;
            const pagination = document.querySelector('[data-type="pagination"]');
            if (!pagination) return;
            const main = document.querySelector("main");
            const next = pagination.querySelector('[rel="next"]');
            const last = pagination.querySelector('[rel="last"]');
            if (!last) return;
            const end = last.title === "hidden";
            const asynchronous = document.querySelectorAll("[data-type='pagination']").length !== 1;
            if (end || asynchronous) return pagination.remove();
            if (threshold && pagination.remove() === undefined) {
                fetch(next.href, "GET", function(http) {
                    const page = new DOMParser().parseFromString(http.responseText, "text/html");
                    const items = Array.prototype.slice.call(page.querySelector("main").children);
                    const paginate = page.querySelector('[data-type="pagination"]');
                    for(var i = 0; i < items.length; i++)main.appendChild(items[i]);
                    self.document.dispatchEvent(new CustomEvent("FormsCustomEvent", {
                        bubbles: true
                    }));
                    main.after(paginate);
                }, function(http) {
                    console.warn("WARNING: Switching to native pagination", http);
                    main.insertAdjacentElement("afterend", pagination);
                    abort = true;
                });
            }
        });
    });
})();
(function() {
    const relative = new Intl.RelativeTimeFormat("en", {
        localeMatcher: "best fit",
        numeric: "always",
        style: "long"
    });
    function viewport(element) {
        const options = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : {
            offset: 250
        };
        const view = element.getBoundingClientRect();
        return view.top >= -options.offset && view.left >= -options.offset && view.bottom <= (self.innerHeight || document.documentElement.clientHeight) + options.offset && view.right <= (self.innerWidth || document.documentElement.clientWidth) + options.offset;
    }
    const date = function(update) {
        const elements = document.querySelectorAll("time");
        for(let i = 0; i < elements.length; ++i){
            const offscreen = !viewport(elements[i]);
            const hidden = elements[i].offsetParent === null;
            if (update === "viewport" && (offscreen || hidden)) continue;
            (function(element) {
                try {
                    if (element.dataset.type === "disabled") return;
                    const time = new Date(element.dateTime) || new Date();
                    const interval = (new Date().getTime() - time.getTime()) / 1000;
                    const seconds = Math.round(interval);
                    const minutes = Math.round(seconds / 60);
                    const hours = Math.round(minutes / 60);
                    const days = Math.round(hours / 24);
                    const past = Math.sign(seconds) === 1;
                    const future = Math.sign(seconds) === -1;
                    let tiny = function(string, place) {
                        return string.split(" ").slice(0, place).join(" ") + string.split(" ")[place].charAt(0);
                    };
                    if (element.dataset.type === "default") {
                        tiny = function(string) {
                            return string;
                        };
                    }
                    if (element.dataset.type === "localDate") {
                        return element.textContent = new Intl.DateTimeFormat([], {
                            dateStyle: "medium"
                        }).format(time).replace(",", "");
                    }
                    if (element.dataset.type === "localTime") {
                        return element.textContent = new Intl.DateTimeFormat([], {
                            hour12: false,
                            timeStyle: "short"
                        }).format(time) + " " + new Intl.DateTimeFormat([], {
                            timeZoneName: "short"
                        }).format(time).split(" ")[1];
                    }
                    if (past) {
                        if (seconds <= 60) {
                            return element.textContent = tiny(relative.format(-1 * seconds, "second"), 1);
                        }
                        if (minutes <= 120) {
                            return element.textContent = tiny(relative.format(-1 * minutes, "minute"), 1);
                        }
                        if (hours <= 48) {
                            return element.textContent = tiny(relative.format(-1 * hours, "hour"), 1);
                        }
                        if (days <= 3) {
                            return element.textContent = tiny(relative.format(-1 * days, "day"), 1);
                        }
                    }
                    if (future) {
                        if (-1 * days >= 4) {
                            return element.textContent = tiny(relative.format(-1 * days, "day"), 2);
                        }
                        if (-1 * hours >= 3) {
                            return element.textContent = tiny(relative.format(-1 * hours, "hour"), 2);
                        }
                        if (-1 * minutes >= 2) {
                            return element.textContent = tiny(relative.format(-1 * minutes, "minute"), 2);
                        }
                        if (-1 * seconds >= 1) {
                            return element.textContent = tiny(relative.format(-1 * seconds, "second"), 2);
                        }
                    }
                } catch (error) {
                    console.error("ERROR: Relative time resolution failed", error);
                }
            })(elements[i]);
        }
    };
    const substitution = setInterval(date, 4);
    [
        "scroll",
        "URLChangedCustomEvent"
    ].forEach(function(event) {
        self.addEventListener(event, function() {
            date("viewport");
        });
    });
    self.addEventListener("DOMContentLoaded", function() {
        setTimeout(function() {
            clearInterval(substitution);
            setInterval(function() {
                date("viewport");
            }, 1000);
        }, 1000);
    });
})();
console.log("INFO: Surface Control Complete");