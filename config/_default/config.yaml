---
baseURL:
theme: default
title: A Micro Blog

summaryLength: 1
enableEmoji: true
buildDrafts: true
buildExpired: true
languageCode: en-us
enableRobotsTXT: true

taxonomies:
  tag: tags

menu:
  main:
  - name: Home
    url: /
    identifier: tabler/home
    weight: 1
  - name: Tags
    url: /tags/
    identifier: tabler/tag
    weight: 2
    icon: home
  - name: Archives
    url: /archives/
    identifier: tabler/archive
    weight: 3
  - name: Authors
    url: /authors/
    identifier: tabler/users
    weight: 4
  - name: Settings
    url: /settings/
    identifier: tabler/settings
    weight: 5
  - name: Sources
    url: /sources/
    identifier: tabler/git-fork
    weight: 6
  - name: RSS Feed
    url: /rss.xml
    identifier: tabler/rss
    weight: 7
  - name: JSON Feed
    url: /index.json
    identifier: tabler/circle
    weight: 8

params:
  site:
    offline: false
    production: false
    referrer: no-referrer
    robots: index,follow
    canonical:
  webmanifest:
    start_url: /
    name: Micro Blog
    short_name: Micro
    theme_color: '#ffffff'
    background_color: '#ffffff'
    display: standalone
    description: A Micro Blog
    logo: data/media/logo.png
  csp:
    block-all-mixed-content: ''
    base-uri: ["'self'"]
    connect-src: ["'self'"]
    default-src: ["'self'"]
    font-src: ["'self'"]
    form-action: ["'self'", lite.duckduckgo.com]
    frame-src: ["'self'", www.youtube-nocookie.com, en.m.wikipedia.org, odysee.com]
    img-src: ["'self'", 'data:', http://preview.test]
    manifest-src: ["'self'", 'data:']
    media-src: ["'self'"]
    object-src: ["'self'"]
    script-src-elem: ["'self'"]
    script-src: ["'self'"]
    style-src: ["'self'", "'unsafe-inline'", http://preview.test]
  search:
    verification:
      baidu-site-verification:
      google-site-verification:
      msvalidate.01:
      yandex-verification:
  authors:
    list:
    - canory
    - default
    - nobody
    default:
      footer: A Micro Blog
  segments:
    render:
      pagers: [home, section, taxonomy, term]
      kinds: [page, home, section, taxonomy, term]
      labels:
        page: [baseof.html]
        home: [baseof.html]
        section: [baseof.html, gallery.html, webfeeds.html, weblinks.html]
        taxonomy: [baseof.html]
        term: [baseof.html]

related:
  includeNewer: false
  indices:
  - name: tags
    weight: 80
  - name: date
    weight: 10
  threshold: 80
  toLower: false

mediaTypes:
  application/xslt+xml:
    suffixes:
    - xsl

outputFormats:
  html:
    baseName: index
    mediaType: text/html
  embed:
    baseName: embed
    mediaType: text/html
  plain:
    baseName: index
    mediaType: text/plain
  json:
    baseName: index
    mediaType: application/json
  rss:
    baseName: rss
    mediaType: application/rss+xml
  xslt.rss:
    baseName: rss
    mediaType: application/xslt+xml
  xslt.sitemap:
    baseName: sitemap
    mediaType: application/xslt+xml
  authors:
    path: authors
    baseName: index
    mediaType: text/html
  authors.section:
    baseName: authors
    mediaType: text/html
  archives:
    path: archives
    baseName: index
    mediaType: text/html
  settings:
    path: settings
    baseName: index
    mediaType: text/html
  sources:
    path: sources
    baseName: index
    mediaType: text/html
  gallery:
    baseName: gallery
    mediaType: text/html
  webfeeds:
    baseName: webfeeds
    mediaType: text/html
  weblinks:
    baseName: weblinks
    mediaType: text/html

outputs:
  home:
  - html
  - rss
  - xslt.rss
  - xslt.sitemap
  - json
  - authors
  - authors.section
  - sources
  - archives
  - settings
  section:
  - html
  - rss
  - gallery
  - webfeeds
  - weblinks
  taxonomy:
  - html
  - rss
  term:
  - html
  - rss
  page:
  - html
  - plain
  - embed

caches:
  assets:
    dir: :resourceDir/_gen
    maxAge: -1
  images:
    dir: :resourceDir/_gen
    maxAge: -1
  getcsv:
    dir: :resourceDir/caches
    maxAge: 12h
  getjson:
    dir: :resourceDir/caches
    maxAge: 12h
  getresource:
    dir: :resourceDir/caches
    maxAge: 12h
  modules:
    dir: :resourceDir/caches
    maxAge: -1

module:
  mounts:
  - source: content
    target: content
  - source: static
    target: static
  - source: layouts
    target: layouts
  - source: data
    target: data
  - source: assets
    target: assets
  - source: i18n
    target: i18n
  - source: archetypes
    target: archetypes

  - source: content
    target: static/data/generates/content
  - source: themes/default/layouts
    target: static/sources/files
  - source: data
    target: static/data
  - source: public/data/generates/content
    target: content
  - source: public/data/generates/authors
    target: data/authors
  - source: public
    target: assets/public

sitemap:
  changefreq: weekly
  filename: sitemap.xml
  priority: 0.5

markup:
  asciidocExt:
    attributes: {}
    backend: html5
    extensions: []
    failureLevel: fatal
    noHeaderOrFooter: true
    preserveTOC: false
    safeMode: unsafe
    sectionNumbers: false
    trace: false
    verbose: false
    workingFolderCurrent: false
  defaultMarkdownHandler: goldmark
  goldmark:
    extensions:
      definitionList: true
      footnote: true
      linkify: true
      linkifyProtocol: https
      strikethrough: true
      table: true
      taskList: true
      typographer: true
    parser:
      attribute:
        block: false
        title: true
      autoHeadingID: true
      autoHeadingIDType: github
      wrapStandAloneImageWithinParagraph: false
    renderer:
      hardWraps: false
      unsafe: false
      xhtml: true
  highlight:
    anchorLineNos: true
    codeFences: true
    guessSyntax: true
    hl_Lines: ''
    hl_inline: false
    lineAnchors: ''
    lineNoStart: 1
    lineNos: false
    lineNumbersInTable: false
    noClasses: false
    noHl: false
    style:
    tabWidth: 4
  tableOfContents:
    endLevel: 3
    ordered: false
    startLevel: 2
