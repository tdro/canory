+++
date    = "2022-03-05T03:45:51+00:00"
lastmod = "2022-03-05T03:45:51+00:00"
tags    = [ "docs" ]
+++

Somehow you've ended up here. It's funny how that works. I'm responsible for
explaining how to get started.

GIT CLONE.

{{% version clone=true %}}

EXECUTE HUGO TWICE. HUGO VERSION >= {{% version hugo=true %}} AND < 0.123.0

```shell
cd canory
hugo && hugo
```

SERVE HTML. PREVIEW USING ANY HTTP SERVER.

```shell
cd public
python -m http.server --bind 127.0.0.1 8124
busybox httpd -f -p 127.0.0.1:8125
php -S 127.0.0.1:8126
ruby -run -e httpd . -p 8127
caddy file-server --listen :8128
npx serve --listen 8129
```
