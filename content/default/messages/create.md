+++
date    = "2022-03-13T04:24:37+00:00"
lastmod = "2022-03-13T04:24:37+00:00"
tags    = [ "docs" ]
+++

"Bootstrap" the authors. Add a name to the authors' list in the `toml/json/yaml`
configuration file. We tried to create a somebody but all we could muster was a
[nobody](/nobody).

```yaml
authors:
  list:
  - canory
  - default
  - nobody
```

To complete the process, read and follow [nobody's profile](/nobody), then
create nobody's home in the content directory.

```
content
|__ nobody
    |__ messages
        |__ new.md
```
