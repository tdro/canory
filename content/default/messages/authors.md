+++
date    = "2022-03-14T04:35:51+00:00"
lastmod = "2022-03-14T04:35:51+00:00"
tags    = [ "docs" ]
+++

Beep beep beep. Another user? Multiple users are available by setting a specific
author. [@canory](/canory) thinks this is cool but doesn't know any better ---
it's hacks all the way down.
