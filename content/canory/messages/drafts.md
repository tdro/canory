+++
date    = "2022-03-04T02:44:51+00:00"
lastmod = "2022-03-04T02:44:51+00:00"
tags    = [ "docs" ]
draft   = true
+++

Draft those messages, you message drafter. Get a feel of how it delivers before
committing the deed. Only reveal your drafts in a local environment --- don't
write drafts live on the `www` unless you like living on the edge.

```yaml {options="hl_lines=9-17",caption="Inside your config.toml, config.json, or config.yaml"}
---
buildDrafts: false
```
