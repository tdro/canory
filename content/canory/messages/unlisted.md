+++
date     = "2022-03-08T02:16:51+00:00"
lastmod  = "2022-03-08T02:16:51+00:00"
tags     = [ "docs" ]
unlisted = true
+++

So you're a bit bold. You have the cooking skills needed to stay in the kitchen.
This message is not propagated and hopefully not indexed. The communication
stays mostly off the record and will not spontaneously blow up.
