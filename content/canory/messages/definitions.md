+++
date    = "2022-03-17T04:45:51+00:00"
lastmod = "2022-03-17T04:45:51+00:00"
tags    = [ "docs" ]
+++

There's no infinite scrolling here. It enfeebles the psyche. Here's a list of
definitions. You have received enlightenment; heed my words.

**Infinite**
: Subject to no limitation or external determination.

**Enfeeble**
: To make feeble: to deprive of strength.

**Psyche**
: The totality of elements forming the mind.
