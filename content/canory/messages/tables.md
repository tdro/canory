+++
date    = "2022-03-04T02:44:51+00:00"
lastmod = "2022-03-04T02:44:51+00:00"
tags    = [ "docs" ]
+++

Tables are the most universally understood data presentation format but...

| Tables      |      Are       | Boring |
|:------------|:--------------:|-------:|
| column 1 is | left--aligned  |    123 |
| column 2 is |    centered    |     12 |
| column 3 is | right--aligned |      1 |

