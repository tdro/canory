+++
date       = "2022-03-08T02:20:51+00:00"
lastmod    = "2022-03-08T02:20:51+00:00"
expirydate = "2122-04-29T22:55:17+00:00"
tags       = [ "docs" ]
slug       = "cf1a1feb1"
+++

{{< disclose >}}
Extraterrestrials care not about speech. According to my
[lackey](https://thedroneely.com), the Internet is serious business for humans.
{{</ disclose >}}

Professor Wiio says;
{{< quote
  "*If a message can be interpreted in several ways, it will be interpreted in a manner that maximizes the damage.*"
  "https://en.wikipedia.org/wiki/Wiio%27s_laws"
>}}

Limit or boost virality and damage by self destructing the source of an imagined
controversy. This is not to be indexed --- search engine bots
{{< smallcaps "shall not pass" >}} (hopefully), neither should this message
propogate through any feeds. This
{{< markdown text="hidden message" title="hidden in plain sight" >}} will only
disappear in my timeline because I'm an intergalactic time traveler.

![Isaac Asimov](/canory/media/internet-is-serious.jpg)
