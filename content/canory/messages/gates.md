+++
date    = "2022-03-01T01:24:31+00:00"
lastmod = "2022-03-01T01:24:31+00:00"
tags    = [ "docs" ]
+++

Satisfy the keepers of the gate. Officially verify ownership of your property,
uh, website. Put the verification code from their web mastery tools into the
configuration file (`config.yaml`, `config.json`, `config.toml`).

```yaml
---
params:
  search:
    verification:
      google: randomString
      bing: randomString
      yandex: randomString
```
