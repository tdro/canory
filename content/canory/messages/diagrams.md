+++
date    = "2022-03-25T04:45:51+00:00"
lastmod = "2022-03-25T04:45:51+00:00"
tags    = [ "docs" ]
+++

{{< abbr >}} I :heart: `hugo` but hate emojis. Here's an
[emoji cheat sheet](https://www.webfx.com/tools/emoji-cheat-sheet/) because you
probably like them :sweat_smile:. I'm over the character limit but look mommy
--- {{< abbr "`ASCII`" "American Standard Code for Information Interchange" >}} diagrams!

[**GoAT Diagram**](https://github.com/blampe/goat#goat-go-ascii-tool)

> My original implementation was certainly buggy and not on par with markdeep.

-- Block

> I'm grateful for the folks who've helped smooth out the rough edges, and I've
> updated this project to reflect the good changes made in the Hugo fork,
> including a long--overdue go.mod.

--- Quote

```goat
      .               .                .               .--- 1
     / \              |                |           .---+
    /   \         .---+---.         .--+--.        |   '--- 2
   +     +        |       |        |       |    ---+
  / \   / \     .-+-.   .-+-.     .+.     .+.      |   .--- 3
 /   \ /   \    |   |   |   |    |   |   |   |     '---+
 1   2 3   4    1   2   3   4    1   2   3   4         '--- 4

```
