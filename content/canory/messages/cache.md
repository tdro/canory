+++
date    = "2022-02-27T20:18:43+00:00"
lastmod = "2022-02-27T20:18:43+00:00"
tags    = [ "docs" ]
+++

Resource abuse is not good --- cache responsibly. In the configuration file
(config.yaml, config.json, config.toml) set the cache expiry time. The default
is `12h` (12 hours) for remote fetches and `-1` (forever) for assets, images,
and modules. The time scales are seconds (`s`), minutes (`m`), and hours (`h`).
Turn off a cache by setting a max age of `0`.

```yaml {options="hl_lines=9-17",caption="Caching Options"}
---
caches:
  assets:
    dir: :resourceDir/_gen
    maxAge: -1
  images:
    dir: :resourceDir/_gen
    maxAge: -1
  getcsv:
    dir: :resourceDir/caches
    maxAge: 12h
  getjson:
    dir: :resourceDir/caches
    maxAge: 12h
  getresource:
    dir: :resourceDir/caches
    maxAge: 12h
  modules:
    dir: :resourceDir/caches
    maxAge: -1
```
