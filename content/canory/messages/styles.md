+++
date    = "2022-03-23T04:45:51+00:00"
lastmod = "2022-03-23T04:45:51+00:00"
tags    = [ "docs" ]
+++

**Subtlety** _wins_ arguments. Other times an **_impassioned plea_** gets the
job done. Highlight `bits` of `monospaced` text to look sophisticated.
