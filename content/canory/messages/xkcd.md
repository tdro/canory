+++
date    = "2022-03-18T04:45:51+00:00"
lastmod = "2022-03-18T04:45:51+00:00"
tags    = [ "docs" ]
+++

Hosting and linking content is copyright radioactive. Be a good netizen and
avoid hotlinking,
[but do it when it's acceptable](https://xkcd.com/license.html) (it's cached
anyway).

![Debugging](https://imgs.xkcd.com/comics/debugging.png "
  xkcd: [Debugging](https://xkcd.com/1722)"
)
