+++
date    = "2022-03-12T02:44:51+00:00"
lastmod = "2022-03-12T02:44:51+00:00"
tags    = [ "docs" ]
+++

Speaking of lists. Everyone likes a good list. Spur ridicule and controversy by
ranking things. It's that easy.

  1. C
  2. Bash
  3. Perl

  ---

  10. JavaScript
  11. PHP
  12. Java
