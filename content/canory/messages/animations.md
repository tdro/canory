+++
date     = "2022-03-16T05:41:52+00:00"
lastmod  = "2022-03-16T05:41:52+00:00"
tags    = [ "docs" ]
+++

Text animations are out of style. Wait... {{< animate waggle "nu-uh" >}}, just
checked the clock --- and would ya know they're back in {{< animate twirl style
>}}.

- {{< animate roll Observe >}} the {{< animate hang signs. >}} Reality {{< animate flip bingo >}}.
- We are {{< animate float floating >}} on magic pixie dust. Very {{< animate tremble scary. >}}
- My ego is under {{< animate vibrate attack >}}. Oh {{< animate jiggle "noo!" >}}
- Just pick {{< animate hop me >}} please, {{< animate twitch pretty >}} please..
- It's all just an {{< animate skip animation >}}. Frames per second: {{< animate rattle "Infinite!" >}}
- Command line {{< animate wave flags. >}} Sorcery and {{< animate distort runes. >}}
- Resist the {{< animate shake struggle >}}. You must {{< animate jump fight >}}. Now {{< animate squeeze breathe. >}}
- Big {{< animate shrink weak >}}, Little {{< animate grow strong. >}}
