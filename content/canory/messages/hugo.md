+++
date    = "2022-03-26T04:45:51+00:00"
lastmod = "2022-03-26T04:45:51+00:00"
tags    = [ "docs" ]
+++

This micro blog is powered by Hugo; a static site generator that has lots of
features.

![Hugo's landing page](/canory/media/hugo-website.png "
  Hugo's [homepage](https://gohugo.io/)."
)
