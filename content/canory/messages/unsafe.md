+++
date    = "2022-03-09T02:43:51+00:00"
lastmod = "2022-03-09T02:43:51+00:00"
tags    = [ "docs" ]
+++

{{< disclose />}}

Is it safe? Well this message ain't.
{{< quote
  "Evil communications corrupt good manners"
  "https://www.kingjamesbibleonline.org/1-Corinthians-15-33/"
>}}. Grace your hearers with a warning before things get unsavory.
Shoo.

{{< youtube "gk8XGnKLhfU" "Monopoly Mermaid Monday" >}}
