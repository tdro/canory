+++
date    = "2022-03-01T01:26:21+00:00"
lastmod = "2022-03-01T01:26:21+00:00"
tags    = [ "docs" ]
+++

# Weights and Spans?

I was once told spans were worth their weight in gold. Two sentences in line,
nine seconds with time.

## Secondary Heading

So you've made it this far, you're excellent, on par! Two paragraphs or no, just
a little more to not go.

### Tertiary Heading

Saepe perspiciatis molestiae iste at quia sequi. Omnis itaque sint ut nihil
molestiae quis ut at.

## Conclusion

You've skipped to the end and thus failed, my friend. You didn't know?
Conclusions and Latin are just for show!
