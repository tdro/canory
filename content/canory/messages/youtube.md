+++
date     = "2022-03-16T04:45:51+00:00"
lastmod  = "2022-03-16T04:45:51+00:00"
tags     = [ "docs" ]
+++

Netizens think that there are only 5 websites. Here's a video from one of them.
Pat yourself on the back, you have reached [the end of the
Internet](https://hmpg.net/).

{{< youtube id=9LoVUbYSv5M muted=1 >}}
