+++
date    = "2022-02-05T02:20:51+00:00"
lastmod = "2022-03-29T22:55:17+00:00"
tags    = [ "docs" ]
weight  = 1
marked  = true
+++

This message is pinned for reasons unknown. For a quick start guide,
[@default](/default/messages/quickstart/#default-messages-quickstart) might be able to help
you. The [#docs](/tags/docs) explain what we are currently capable of doing.
{{< mark Warning />}}: This micro blog is _remarkably_ experimental, don't hurt
yourself.
