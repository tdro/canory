+++
date    = "2022-03-22T04:45:51+00:00"
lastmod = "2022-03-22T04:45:51+00:00"
tags    = [ "docs" ]
+++

Demonstrate your brilliance. Embed code with impeccable taste and beautiful
style.

```lisp {options="hl_lines=1,linenos=1",caption="Not too sure what any of this means."}
;;; Numbers

9999999999999999999999 ; integers
#b111                  ; binary => 7
#o111                  ; octal => 73
#x111                  ; hexadecimal => 273
3.14159s0              ; single
3.14159d0              ; double
1/2                    ; ratios
#C(1 2)                ; complex numbers
```
