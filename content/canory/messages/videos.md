+++
date    = "2022-03-20T04:45:51+00:00"
lastmod = "2022-03-20T04:45:51+00:00"
tags    = [ "docs" ]
+++

Here's a video that warms my dear heart. Showing videos to others makes you
appear friendly.

{{< video caption="Big Buck Bunny" start=40 end=55 >}}
