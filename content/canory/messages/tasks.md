+++
date    = "2022-03-13T03:44:51+00:00"
lastmod = "2022-03-13T03:44:51+00:00"
tags    = [ "docs" ]
+++

We get things done around here! The first step is to write down our goals for
today. You can schedule posts by setting a date into the future.

- [x] Write Task List
- [x] Check Task List
- [ ] Forget About Task List &trade; 

