+++
date    = "2022-03-02T02:44:51+00:00"
lastmod = "2022-03-02T02:44:51+00:00"
tags    = [ "docs" ]
+++

Check out this party trick. This thing can quote other messages.
{{< spoiler >}}
Technically... the instructions from the manufacturer was not to do this --- but
rules are made to be broken... right?
{{< /spoiler >}}

{{< self "/canory/messages/self/embed.html" >}}
