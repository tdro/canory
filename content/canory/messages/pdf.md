+++
date    = "2022-03-07T02:20:51+00:00"
lastmod = "2022-03-07T02:20:51+00:00"
tags    = [ "docs" ]
+++

I heard you like {{< abbr `PDFs` "Portable Document Format" >}} but hate
accidently downloading them.
[Charles Dickens](https://www.gutenberg.org/ebooks/98) is a
[word wizard](/canory/media/2city12p.pdf).

{{< attach >}}
