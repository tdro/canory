+++
date    = "2022-03-10T02:44:51+00:00"
lastmod = "2022-03-10T02:44:51+00:00"
tags    = [ "docs" ]
+++

Is your `JavaScript` functional? Double[^1] click a word and hold the
{{< kbd D >}} key to look it up in a dictionary. Select a phrase, and hold the
{{< kbd S >}} key to look it up in a search engine. Smartphone users can ignore
this and keep scrolling --- that's all they're good for.

[^1]: First saw this idea [here](https://arachnoid.com/administration/index.html)
      and promptly boosted it.
