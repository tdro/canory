+++
date    = "2022-03-06T02:20:51+00:00"
lastmod = "2022-03-06T02:20:51+00:00"
tags    = [ "docs" ]
+++

Reason, meaning, and knowledge are the ground truths of society. But, only after
all other methods have failed. Invoke the world's most used encyclopedia...

{{< mark Warning />}}:
Article text may *change* as it appears --- the pedia's not guaranteed to be
consistent across discrete spacetimes. If the below does not say
"knowledge" you've been had.

{{< wikipedia >}}

