+++
date    = "2022-03-24T04:45:51+00:00"
lastmod = "2022-03-24T04:45:51+00:00"
tags    = [ "docs" ]
+++

Sometimes I'll {{< mark "highlight stuff" />}} to draw attention to my fine
eloquence. If I don't think things through, I'll ~~hide the what I wrote~~.
Whoops, let's just {{< ins "correct the record." >}}
{{< spoiler "Move along --- nothing to see here." />}}
{{< spoiler "Still nothing." />}}
