+++
date    = "2022-03-07T02:19:12+00:00"
lastmod = "2022-03-07T02:19:12+00:00"
tags    = [ "docs" ]
+++

I'll keep it short. The introduction of _A Tale of Two Cities_ by Charles
Dickens --- enjoy!

---

{{< disclose >}}
It was the best of times, it was the worst of times.
{{</ disclose >}}

It was the age of wisdom, it was the age of foolishness, it was the epoch of
belief, it was the epoch of incredulity.

{{< disclose >}}
It was the season of Light, it was the season of Darkness.
{{</ disclose >}}

It was the spring of hope, it was the winter of despair, we had everything
before us, we had nothing before us.

{{< disclose >}}
We were all going direct to Heaven, we were all going direct the other way.
{{</ disclose >}}

In short, the period was so far like the present period, that some of its
noisiest authorities insisted on its being received, for good or for evil, in
the superlative degree of comparison only.
