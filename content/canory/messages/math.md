+++
date    = "2022-03-01T01:25:42+00:00"
lastmod = "2022-03-01T01:25:42+00:00"
tags    = [ "docs" ]
+++

First nothing, then the infinity wars. You know; the miracle of the fallacy
_&agrave; la_ generality.

```mathml { caption="Sounds about right." }
<math xmlns="http://www.w3.org/1998/Math/MathML" alttext="\begin{align*} & 0 = 0 \times 1\\ & 0 = 0 \times 2\\ & 0 = 0 \times 3 \end{align*}" display="block" >
  <mtable displaystyle="true" columnalign="right">
    <mtr>
      <mtd>
        <mrow>
          <mn>0</mn>
          <mo>=</mo>
          <mn>0</mn>
          <mo>×</mo>
          <mn>1</mn>
        </mrow>
      </mtd>
    </mtr>
    <mtr>
      <mtd>
        <mrow>
          <mn>0</mn>
          <mo>=</mo>
          <mn>0</mn>
          <mo>×</mo>
          <mn>2</mn>
        </mrow>
      </mtd>
    </mtr>
    <mtr>
      <mtd>
        <mrow>
          <mn>0</mn>
          <mo>=</mo>
          <mn>0</mn>
          <mo>×</mo>
          <mn>3</mn>
        </mrow>
      </mtd>
    </mtr>
  </mtable>
</math>
```

```mathml { caption="Alien levels of literal meta--abstraction." }
<math xmlns="http://www.w3.org/1998/Math/MathML" alttext="\begin{align*} & 0 = 0\\ & 0 \times 1 = 0 \times 3 \end{align*}" display="block">
  <mtable displaystyle="true" columnalign="right left">
    <mtr>
      <mtd>
        <mrow>
          <mn>0</mn>
          <mo>=</mo>
          <mn>0</mn>
        </mrow>
      </mtd>
    </mtr>
    <mtr>
      <mtd>
        <mrow>
          <mn>0</mn>
          <mo>×</mo>
          <mn>1</mn>
          <mo>=</mo>
          <mn>0</mn>
          <mo>×</mo>
          <mn>3</mn>
        </mrow>
      </mtd>
    </mtr>
  </mtable>
</math>
```

```mathml { caption="Unity. Don't try this at home, kids." }
<math xmlns="http://www.w3.org/1998/Math/MathML" alttext="\begin{align*} & 1 = 3 \end{align*}" display="block">
  <mtable displaystyle="true" columnalign="right left">
    <mtr>
      <mtd>
        <mrow>
          <mn>1</mn>
          <mo>=</mo>
          <mn>3</mn>
        </mrow>
      </mtd>
    </mtr>
  </mtable>
</math>
```
