+++
date    = "2022-03-14T04:45:51+00:00"
lastmod = "2022-03-14T04:45:51+00:00"
tags    = [ "docs" ]
+++

[//]: # "That's a comment alright, hiding in plain sight."

The `metadata` :arrow_up: menu has a link to the raw
[Markdown](https://www.markdownguide.org/cheat-sheet/). The file
{{< markdown >}} generates this message. I might as well show my internals.
Casual users will frown, power users will delight.

{{< markdown embed=true >}}
