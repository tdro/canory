+++
date    = "2022-03-19T04:45:51+00:00"
lastmod = "2022-03-19T04:45:51+00:00"
tags    = [ "docs" ]
+++

Embedding a `gif` hosted from an external site is easy. I heard it might be
illegal someday. Yak shaving and legal jangling are the worst, truly.

{{< video-imgur rQIb4Vw >}}
