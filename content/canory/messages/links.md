+++
date    = "2022-03-15T03:45:51+00:00"
lastmod = "2022-03-15T03:45:51+00:00"
tags    = [ "docs" ]
+++

This individual is old enough to remember that the Internet is
a vast place.

{{< link "https://web.archive.org/web/20200111092601/https://twitter.com/tveastman/status/1069674780826071040" >}}
