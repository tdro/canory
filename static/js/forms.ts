(function () {

  const cookiesDisabled = !navigator.cookieEnabled;

  if (cookiesDisabled) return console.warn("WARNING: Form state cannot persist due to cookie restrictions");

  const storage = document.createEvent("Event");
  storage.initEvent("storage", true, true);

  ["pageshow", "FormsCustomEvent", "URLChangedCustomEvent", "DOMContentLoaded"].forEach(function (event) {
    self.addEventListener(event, function (event) {
      const input = Array.prototype.slice.call(document.querySelectorAll("form input"));
      const select = Array.prototype.slice.call(document.querySelectorAll("form select"));
      const textarea = Array.prototype.slice.call(document.querySelectorAll("form textarea"));
      const summary = Array.prototype.slice.call(document.querySelectorAll("details summary"));

      const states = input.concat(select).concat(textarea);

      for (var i = 0; i < states.length; i++) {
        const state = states[i];

        if (localStorage[state.id]) {
          if (state.type === "radio" || state.type === "checkbox") {
            if (localStorage[state.id] === "on") state.checked = true;
          } else state.value = localStorage[state.id];
        }

        if (state.dataset.listener === "active") break;

        state.setAttribute("data-listener", "active");

        state.addEventListener("change", function (event) {

          // console.log("INFO: STATE:",   event.target);
          // console.log("INFO: ID:",      event.target.id);
          // console.log("INFO: NAME:",    event.target.name);
          // console.log("INFO: TYPE:",    event.target.type);
          // console.log("INFO: VALUE:",   event.target.value);
          // console.log("INFO: CHECKED:", event.target.checked);

          localStorage[event.target.id] = event.target.value;

          const group = document.querySelectorAll("input[name='".concat(event.target.name, "']"));

          for (var j = 0; j < group.length; j++) {
            const member = group[j];
            if ((member.type === "radio" || member.type === "checkbox") && !member.checked) {
              localStorage[member.id] = "off";
            }
          }
          self.dispatchEvent(storage);
        });
      }

      for (var k = 0; k < summary.length; k++) {
        let child = summary[k];
        let details = child.parentElement;

        if (details.id && details.nodeName === "DETAILS") {
          sessionStorage[details.id] === "false" && details.removeAttribute("open");
          sessionStorage[details.id] === "true"  && details.setAttribute("open", true);

          child.addEventListener("click", function (event) {
            let child = (event.target.nodeName === "SUMMARY" && event.target)
                        || event.target.parentElement;
            let details = child.parentElement;
            if (details.id && details.nodeName === "DETAILS") {
              sessionStorage[details.id] = !details.open;
            }
          });
        }
      }
    });
  });

  ["storage"].forEach(function (event) {
    self.addEventListener(event, function () {
      let stylesheet;

      stylesheet = document.querySelector('link[href$="default-simple.css"]')

      if (localStorage["config.layout.simple"]  === "on")  stylesheet.rel = "stylesheet"
      if (localStorage["config.layout.default"] === "on")  stylesheet.rel = "alternate stylesheet"

      stylesheet = document.querySelector('link[href$="default-fast.css"]')

      if (localStorage["config.navigation.fast"] === "on") stylesheet.rel = "stylesheet"
      if (localStorage["config.navigation.slow"] === "on") stylesheet.rel = "alternate stylesheet"

      for (var i = 0; i < document.styleSheets.length; i++) {
        let stylesheet = document.styleSheets[i];
        if (!stylesheet.href || !stylesheet.href.startsWith(self.origin)) continue;
        for (var k = 0; k < stylesheet.rules.length; k++) {
          let media = stylesheet.rules[k].media;
          if (media === undefined || !media.mediaText.startsWith("(prefers-color-scheme:")) continue;
          switch (true) {
            case (localStorage["config.theme.light"] === "on"):
              media.mediaText = "(prefers-color-scheme: dark)";
              if (getComputedStyle(document.body).getPropertyValue("color-scheme") === "dark") media.mediaText = "(prefers-color-scheme: light)";
              continue;

            case (localStorage["config.theme.auto"] === "on"):
              media.mediaText = "(prefers-color-scheme: dark)";
              continue;

            case (localStorage["config.theme.dark"] === "on"):
              media.mediaText = "(prefers-color-scheme: light)";
              if (getComputedStyle(document.body).getPropertyValue("color-scheme") === "light") media.mediaText = "(prefers-color-scheme: dark)";
          }
        }
      }
    });
   });

  const early = setInterval(persistence, 4);

  function persistence() {
    if (document.styleSheets.length > 0) {
      self.dispatchEvent(storage);
      clearInterval(early);
    }
  }

  self.addEventListener("DOMContentLoaded", function () {
    self.dispatchEvent(storage);
    clearInterval(early);
  });
})();
