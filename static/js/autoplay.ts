(function () {

  const cookiesDisabled = !navigator.cookieEnabled;

  if (cookiesDisabled) return console.warn("WARNING: Autoplay control disabled due to cookie restrictions");

  function viewport(element) {
      const options = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : {
          offset: { top: -50, left: 0, bottom: -50, right: 0 }
      };
      const view = element.getBoundingClientRect();
      return view.top    >= -options.offset.top
          && view.left   >= -options.offset.left
          && view.bottom <= (self.innerHeight || document.documentElement.clientHeight) + options.offset.bottom
          && view.right  <= (self.innerWidth  || document.documentElement.clientWidth)  + options.offset.right;
  };

  const state = "on";
  const key = "config.video.autoplay";

  if (!localStorage[key]) localStorage[key] = state;

  ["scroll", "DOMContentLoaded", "pointerover", "touchstart", "touchend"].forEach(function (event) {
    self.addEventListener(event, function (event) {

      if (event.type === "pointerover" || event.type === "touchstart" || event.type === "touchend") {
        if (event.target.nodeName === "VIDEO") {
          event.target.setAttribute("data-autoplay", false);
        }
        return;
      }

      let first = true;
      const videos = document.querySelectorAll("video");

      for (let i = 0; i < videos.length; i++) {
        videos[i].autoplay    = true;
        videos[i].controls    = true;
        videos[i].loop        = true;
        videos[i].muted       = true;
        videos[i].playsinline = true;

        videos[i].setAttribute("autoplay",    true);
        videos[i].setAttribute("controls",    true);
        videos[i].setAttribute("loop",        true);
        videos[i].setAttribute("muted",       true);
        videos[i].setAttribute("playsinline", true);

        const onscreen = viewport(videos[i]);
        const uninterrupted = !videos[i].dataset.autoplay;

        if (first && onscreen && uninterrupted && localStorage[key] === state) {
          videos[i].play();
          first = false;
        } else {
          videos[i].pause();
        }
      }
    });
  });
})();
