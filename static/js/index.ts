import "./strict.ts";
import "./pager.ts";
import "./update.ts";
import "./plumber.ts";
import "./instantpage.ts";
import "./contextmenu.ts";
import "./fixedsearch.ts";
import "./autoplay.ts";
import "./hoverfix.ts";
import "./forms.ts";
import "./domfilter.ts";
import "./infinitescroll.ts";
import "./timeago.ts";

console.log("INFO: Surface Control Complete");
