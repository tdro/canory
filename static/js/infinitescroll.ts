(function () {
  const cookiesDisabled = !navigator.cookieEnabled;

  if (cookiesDisabled) return console.warn("WARNING: Native pagination fallback due to cookie restrictions");

  type percent = number;

  function fetch(url, method, callback, fallback) {
    const http = new XMLHttpRequest();
    http.onreadystatechange = function () {
      if (callback && http.readyState === 4) {
        if (http.status === 200) callback(http);
        else fallback(http);
      }
    };
    http.open(method, url);
    http.send();
    return http;
  }

  let abort = false;

  const state = "off";
  const key = "config.scroll.infinite";

  if (!localStorage[key]) localStorage[key] = state;

  ["scroll", "DOMContentLoaded"].forEach(function (event) {
    self.addEventListener(event, function () {
      if (abort || !localStorage[key] || localStorage[key] === state) return;

      const remaining = Math.abs(
          document.documentElement.scrollHeight
        - document.documentElement.clientHeight
        - self.pageYOffset
      );

      const traversed = self.pageYOffset;
      const journey = remaining + traversed;
      const ratio: percent = traversed / journey * 100;
      const threshold = ratio >= 50;
      const pagination = document.querySelector('[data-type="pagination"]');

      if (!pagination) return;

      const main = document.querySelector("main");
      const next = pagination.querySelector('[rel="next"]');
      const last = pagination.querySelector('[rel="last"]');

      if (!last) return;

      const end = last.title === "hidden";
      const asynchronous = document.querySelectorAll("[data-type='pagination']").length !== 1;

      if (end || asynchronous) return pagination.remove();

      if (threshold && (pagination.remove() === undefined)) {

        fetch(next.href, "GET", function (http) {
          const page = (new DOMParser()).parseFromString(http.responseText, "text/html");
          const items = Array.prototype.slice.call(page.querySelector("main").children);
          const paginate = page.querySelector('[data-type="pagination"]');

          for (var i = 0; i < items.length; i++) main.appendChild(items[i]);

          self.document.dispatchEvent(new CustomEvent("FormsCustomEvent", { bubbles: true }));

          main.after(paginate);

        }, function (http) {
          console.warn("WARNING: Switching to native pagination", http);
          main.insertAdjacentElement("afterend", pagination);
          abort = true;
        });
      }
    });
  });
})();
