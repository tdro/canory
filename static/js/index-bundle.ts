import { bundle } from "https://deno.land/x/emit@0.26.0/mod.ts";

function removeSourceMap(text): string {
  return text.replace(/^\/\/# sourceMappingURL.*$/gm, "").trim();
}

const entrypoint = await bundle(
  "./static/js/index.ts",
);

const output = removeSourceMap(entrypoint.code);
const file = Deno.cwd() + "/assets/js/index.js";
const fileSize = ((await Deno.stat(file)).size / 1024).toFixed(2) + "kB";

await Deno.writeTextFile(file, output, {});

console.log("INFO:", `Bundle file:///${file}`);
console.log("INFO:", `Emit   file:///${file} (${fileSize})`);
