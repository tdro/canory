(function () {
  const hide = function (triggers) {
    for (let i = 0; i < triggers.length; i++) {
      triggers[i].checked = false;
    }
  };

  const hideIfClickedOutside = function (menus, triggers, event) {
    for (let i = 0; i < menus.length; i++) {
      const active = triggers[i].checked === true;
      const outside = !menus[i].contains(event.target);
      if (outside && active) hide(triggers);
    }
  };

  self.addEventListener("scroll", function () {
    const triggers = document.querySelectorAll("menu input");
    hide(triggers);
  });

  ["click", "touchstart"].forEach(function (event) {
    self.addEventListener(event, function (event) {
      const menus = document.querySelectorAll("menu");
      const triggers = document.querySelectorAll("menu input");
      hideIfClickedOutside(menus, triggers, event);
    });
  });
})();
