/*
 * Fixed Search Copyright (C) 2020 Heracles Papatheodorou
 * Copyright (C) Craig Mod, Eddie Webb, and Matthew Daly
 * https://gist.github.com/Arty2/8b0c43581013753438a3d35c15091a9f#file-license-md
 * Licence: MIT | https://mit-license.org/
*/

(function () {
  ["DOMContentLoaded", "URLChangedCustomEvent"].forEach(function (event) {
    self.addEventListener(event, function () {

      const Container = function () { return document.querySelector("search-box")        }; const container = Container();
      const Dropdown  = function () { return document.querySelector("search-box ul")     }; const dropdown  = Dropdown();
      const Form      = function () { return document.querySelector("search-box form")   }; const form      = Form();
      const Query     = function () { return document.querySelector("search-box input")  }; const query     = Query();
      const Submit    = function () { return document.querySelector("search-box button") }; const submit    = Submit();

      function first(element)    { if (element) return element.firstElementChild; }
      function last(element)     { if (element) return element.lastElementChild; }
      function next(element)     { return element.parentElement.nextElementSibling.querySelector("a").focus(); }
      function previous(element) { return element.parentElement.previousElementSibling.querySelector("a").focus(); }
      function press(keyname)    { document.dispatchEvent(new KeyboardEvent("keydown", { "key": keyname })); }

      const Action = {
        cancel: function () { Dropdown().setAttribute("hidden", ""); Container().removeAttribute("data-focus"); },
        reveal: function () { Dropdown().removeAttribute("hidden"); Container().setAttribute("data-focus", ""); },
      };

      if (submit === null) return;
      let selected;

      submit.addEventListener("click", function (event) {
        press("ArrowDown");
        container.setAttribute("data-focus", ""); initialize();
        if (selected) { event.preventDefault(); selected.focus(); return selected.click(); }
        first(dropdown).querySelector("a").focus();
        document.activeElement.click();
      });

      ["keyup", "click"].forEach(function (event) {
        form.addEventListener(event, function () {
          if (document.activeElement.nodeName === "A") return selected = document.activeElement;
          selected = undefined;
        });
      });

      form.addEventListener("focusin", function () { container.setAttribute("data-focus", ""); initialize(); });
      form.addEventListener("submit",  function (event) { event.preventDefault(); return false; });

      form.addEventListener("keydown", function (event) {
        if (event.keyCode == 40) { // DOWN
          event.preventDefault();
          if (document.activeElement == query) first(dropdown).querySelector("a").focus();
          else if (document.activeElement == last(dropdown).querySelector("a")) query.focus();
          else next(document.activeElement);
          return;
        }

        if (event.keyCode == 38) { // UP
          event.preventDefault();
          if (document.activeElement == query) last(dropdown).querySelector("a").focus();
          else if (document.activeElement == first(dropdown).querySelector("a")) query.focus();
          else previous(document.activeElement);
          return;
        }

        if (event.keyCode == 13) { // ENTER
          if (dropdown && document.activeElement == query) {
            event.preventDefault();
            first(dropdown).querySelector("a").focus();
            first(dropdown).querySelector("a").click();
          }
          return;
        }

        if (form.contains(event.target)) {
          if (event.keyCode == 27) { // ESC
            event.preventDefault();
            document.activeElement.blur();
            return Action.cancel();
          }
        }

        if (document.activeElement != query) { query.focus(); }
      });

      if (event === "DOMContentLoaded") {
        let scrolls = 0;
        self.addEventListener("scroll", function () {
          if (scrolls > 75) {
            scrolls = 0;
            document.activeElement.blur();
            Action.cancel();
          }
          scrolls++;
        });
        self.addEventListener("click", function (event) {
          if (
            !Form().contains(event.target) &&
            !(document.activeElement === Query()) &&
            !(document.activeElement === Submit())
          ) {
            Action.cancel();
          }
        });
      }

      function fetch(url, callback) {
        const http = new XMLHttpRequest();
        http.onreadystatechange = function () {
          if (http.readyState === 4 && http.status === 200 && callback) callback(http);
        };
        http.open("GET", url);
        http.send();
      }

      /* Load script based on https://stackoverflow.com/a/55451823 */

      function script(url) {
        return new Promise(function (resolve, reject) {
          const script = document.createElement("script");
          script.onerror = reject;
          script.onload = resolve;
          if (document.currentScript) document.currentScript.parentNode.insertBefore(script, document.currentScript);
          else document.head.appendChild(script);
          script.src = url;
        });
      }

      let data = {};
      let boot = true;
      let origin = document.querySelector("meta[name='base-url']");

      const options = { key: ["title"] };

      function isEmpty(obj) { return Object.keys(obj).length === 0; }

      function appendItemsTo(local, remote) {
        const paginated = Object.keys(remote).includes("next_url");
        if (isEmpty(local)) local = remote;
        else local.items = local.items.concat(remote.items);
        if (paginated) fetch(remote.next_url, function (request) { appendItemsTo(local, JSON.parse(request.responseText)); });
        else search(query.value, data.items, options);
        data = local;
      }

      function initialize() {
        if (boot) {
          boot = false;
          if (origin) origin = origin.content.replace(/\/+$/, "");
          else origin = window.location.origin;
          script(origin + "/js/fuzzysort.js")
          .then(function () {
            fetch(origin + "/index.json", function (request) {
              appendItemsTo({}, JSON.parse(request.responseText));
              Action.reveal();
              search("", data.items, options);
            });
            ["keyup", "focusin"].forEach(function (event) {
              query.addEventListener(event, function () {
                if (data.items) search(query.value, data.items, options);
                else { boot = true; initialize(); }
              });
            });
          }).catch(function (error) { console.error("ERROR: Failed to load fuzzy search", error); });
        } else Action.reveal();
      }

      function escape(text) {
        const escaped = document.createElement("textarea");
        escaped.textContent = text;
        return escaped.innerHTML;
      }

      function search(term, data, options) {
        if (typeof fuzzysort !== "object") return;
        let items = "";
        const results = fuzzysort.go(term, data, options);
        if (results.length === 0 && term.length >= 0) {
          let separator = "—";
          if (term.length === 0) separator = "";
          const li = document.createElement("li");
          const a  = document.createElement("a");
          a.textContent = `${term} ${separator} No Results Found`;
          li.appendChild(a);
          items = li.outerHTML;
        } else {
          for (var string in results.slice(0, 10)) {
            const title = results[string].obj.title;
            const href  = results[string].obj.url;
            let highlight = fuzzysort.highlight(fuzzysort.single(escape(term), escape(title)), "<span>", "</span>");
            if (highlight === null) highlight = title;
            const li = document.createElement("li");
            const a  = document.createElement("a");
            a.href = href;
            a.tabIndex = 0;
            a.innerHTML = highlight;
            li.appendChild(a);
            items = items + li.outerHTML;
          }
        }
        if (dropdown.innerHTML !== items) dropdown.innerHTML = items;
      }
    });
  });
})();
