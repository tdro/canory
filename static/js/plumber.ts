/**
 * Plumber based on and inspired by;
 * Dictionary Access Copyright (C) 2006 Paul Lutus
 * https://arachnoid.com/javascript/dictionary_access.js
 * Licence: GPLv2+ | https://www.gnu.org/licenses/gpl-3.0.txt
 */

(function () {
  const options =
    "targetWindow,width=700,height=500,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes";

  self.addEventListener("keydown", function (event) {
    if (event.repeat && event.key === "d") {
      selection(dictionary);
    }
    if (event.repeat && event.key === "s") {
      selection(search);
    }
    if (event.repeat && event.key === "m") {
      selection(manual);
    }
  });

  function selection(execute) {
    let phrase = "" + window.getSelection();
    phrase = phrase.replace(/[!.:?,;"]/g, "");
    phrase = phrase.replace(/^\s*(\S*?)\s*$/g, "$1");
    if (phrase && phrase > "" && phrase.length > 1) {
      execute(phrase);
    }
  }

  function dictionary(word) {
    window.open(
      "https://www.merriam-webster.com/dictionary/" +
        encodeURIComponent(word),
      "Definitions",
      options,
    );
  }

  function search(phrase) {
    window.open(
      "https://lite.duckduckgo.com/lite/?q=" +
        encodeURIComponent(phrase),
      "Search",
      options,
    );
  }

  function manual(program) {
    window.open(
      "https://man.archlinux.org/search?q=" +
        encodeURIComponent(program),
      "Manual",
      options,
    );
  }
})();
