/*
 * Instant Page Copyright (C) 2019-2023 Alexandre Dieulot
 * https://github.com/instantpage/instant.page/blob/master/LICENSE
 * Licence: MIT | https://mit-license.org/
*/

(function () {

  /*/
   *  Element.closest() polyfill
   *  https://developer.mozilla.org/en-US/docs/Web/API/Element/closest
   *  https://github.com/idmadj/element-closest-polyfill#readme
   *  Copyright (C) Abdelmadjid Hammou
   *  Licence: ISC | https://www.isc.org/licenses/
  /*/

  if (typeof Element !== "undefined") {
    if (!Element.prototype.matches) {
      Element.prototype.matches = Element.prototype.msMatchesSelector ||
        Element.prototype.webkitMatchesSelector;
    }
    if (!Element.prototype.closest) {
      Element.prototype.closest = function (s) {
        var el = this;
        do {
          if (el.matches(s)) return el;
          el = el.parentElement || el.parentNode;
        } while (el !== null && el.nodeType === 1);
        return null;
      };
    }
  }

  /*/
   * -----------------------------------------------------------------
  /*/

  function fetch(url, method, callback) {
    const http = new XMLHttpRequest();
    http.onreadystatechange = function () {
      if (http.readyState === 4 && http.status === 200) {
        if (callback) callback(http);
      }
    };
    http.open(method, url);
    http.send();
    return http;
  }

  self.addEventListener("DOMContentLoaded", function () {

    ["mouseout", "mousedown", "touchstart"].forEach(function (event) {
      self.addEventListener(event, function (event) {
        const url = event.target.closest("a");
        if (preloadable(url) === undefined) return;
        preload(url.href);
      });
    });

    function preloadable(url) {
      switch (true) {
        case (url === null || url.href === null): return;
        case (url.origin !== location.origin): return;
        case (["http:", "https:"].includes(url.protocol) === null): return;
        case (url.protocol === "http:" && location.protocol === "https:"): return;
        case (url.hash && url.pathname + url.search == location.pathname + location.search): return;
        default: return true;
      }
    }

    function preload(url) {
      const prefetcher = document.createElement("link");
      prefetcher.rel = "custom-prefetch";
      prefetcher.href = url;
      const selector = 'link[rel="'.concat(prefetcher.rel, '"][href="').concat(prefetcher.href, '"]');
      const prefetched = document.head.contains(document.querySelector(selector));
      if (prefetched) { return; }
      document.head.appendChild(prefetcher);
      fetch(url, "GET", function () {});
    }
  });
})();
