(function () {
  /*/
   *  Element.closest() polyfill
   *  https://developer.mozilla.org/en-US/docs/Web/API/Element/closest
   *  https://github.com/idmadj/element-closest-polyfill#readme
   *  Copyright (C) Abdelmadjid Hammou
   *  Licence: ISC | https://www.isc.org/licenses/
  /*/

  if (typeof Element !== "undefined") {
    if (!Element.prototype.matches) {
      Element.prototype.matches = Element.prototype.msMatchesSelector ||
        Element.prototype.webkitMatchesSelector;
    }
    if (!Element.prototype.closest) {
      Element.prototype.closest = function (s) {
        var el = this;
        do {
          if (el.matches(s)) return el;
          el = el.parentElement || el.parentNode;
        } while (el !== null && el.nodeType === 1);
        return null;
      };
    }
  }

  /*/
   * -----------------------------------------------------------------
  /*/

  const disabled = "0s";

  function walk(children, callback) {
    for (let i = 0; i < children.length; i++) {
      callback(children[i]);
      walk(children[i].children, callback);
    }
  }

  self.addEventListener("mousemove", function (event) {
    if (typeof event.target.closest !== "function") return;
    const tree = event.target.closest("figure") || event.target.closest("article");

    if (tree !== null) {
      walk(tree.children, function (element) {
        const delay = self.getComputedStyle(element).getPropertyValue(
          "transition-delay",
        );
        if (delay !== disabled) {
          element.style.setProperty("visibility", "hidden");
        }
      });

      walk(tree.children, function (element) {
        const delay = self.getComputedStyle(element).getPropertyValue(
          "transition-delay",
        );
        if (delay !== disabled) {
          element.style.removeProperty("visibility");
        }
      });
    }
  });
})();
