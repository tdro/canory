(function () {
  [...document.querySelectorAll("pre.chroma code")]
    .forEach(
      (element) => {
        try {
          const text = element.textContent || "";
          setTimeout(
            async () => {
              await navigator.clipboard.writeText(text);
              console.log("INFO: Code text copied successfully");
            },
            3000,
          );
        } catch (error) {
          console.error("ERROR: Code text copy failed", error);
        }
      },
    );
})();
