let

  name = "nix-shell.canory";

  system = builtins.currentSystem;

  pkgs = import (builtins.fetchTarball {
    url = "https://releases.nixos.org/nixos/unstable/nixos-23.11pre516114.d680ded26da5/nixexprs.tar.xz";
    sha256 = "13cnlhpp3v7jay4jxyyy2d4kxw4ngpz3m00rhj3vlhvf7jl7hr48";
  }) { inherit system; };

  hugo = pkgs.callPackage ({ lib, buildGoModule, fetchgit }:
    buildGoModule rec {
      pname = "hugo";
      version = "0.122.0";
      src = fetchgit {
        rev = "v${version}";
        url = "https://github.com/gohugoio/hugo.git";
        sha256 = "sha256-pnsQo+nSuIlQ6KKTP1z/BZ74zEu9HjYP66hGStPc0pc=";
      };
      doCheck = false;
      proxyVendor = true;
      vendorSha256 = "sha256-aYy0TOfNIqx44UBXJhewvxi+oSAWjmi/32WvI3HJ3MM=";
      tags = [ "extended" ];
      subPackages = [ "." ];
      meta = {
        license = lib.licenses.asl20;
        homepage = "https://gohugo.io";
        description = "A fast and modern static website engine";
      };
  }) { };

  shell = pkgs.writeShellApplication {
    inherit name;
    text = ''
      /usr/bin/env --ignore-environment /bin/sh -c ${
        pkgs.writeScript name ''
          export PS1='\h (${name}) \W \$ '
          export HOME=$PWD
          export HISTFILE=
          export DENO_DIR=vendor
          export DENO_NO_UPDATE_CHECK=1
          export PATH=${pkgs.lib.strings.makeBinPath [
            hugo
            pkgs.busybox
            pkgs.caddy
            pkgs.check-jsonschema
            pkgs.curl
            pkgs.deno
            pkgs.entr
            pkgs.git
            pkgs.gnumake
            pkgs.inotify-tools
            pkgs.jing
            pkgs.libxml2
            pkgs.libxslt
            pkgs.unixtools.column
            pkgs.validator-nu
            pkgs.xmlstarlet
            pkgs.yj
          ]}
          /bin/sh
        ''
      };
    '';
  };

in pkgs.mkShell {
  inherit hugo;
  shellHook = "exec ${shell}/bin/${shell.name}";
}
