{{- printf `<?xml version="1.0" encoding="utf-8"?>` | safeHTML }}
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:atom="http://www.w3.org/2005/Atom"
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"
  version="1.1"
>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes" />
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" data-xml="" data-document="xhtml">
      <head>
        {{ printf `<title><xsl:value-of select="/rss/channel/title" /> Web Feed</title>` | safeHTML }}
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        {{- partial "head-css.html" . -}}
        {{- partial "head-js.html"  . -}}
      </head>
      <body>
        <column-base position="left"></column-base>
        <column-base position="middle">
          <nav>
            <icon-button>
              <a id="back" target="_self">
                <xsl:choose>
                 <xsl:when test="/rss/channel/atom:link[@rel='previous']/@href">
                   <xsl:attribute name="href">
                     <xsl:value-of select="/rss/channel/atom:link[@rel='previous']/@href" />
                   </xsl:attribute>
                 </xsl:when>
                 <xsl:otherwise>
                   <xsl:attribute name="href">/</xsl:attribute>
                 </xsl:otherwise>
                </xsl:choose>
                {{ safeHTML (readFile (print (partial "function-paths.html").static "/icons/feather/arrow-left.svg")) }}
                <small>Back</small>
              </a>
            </icon-button>
            <section>
             <h2><xsl:value-of select="/rss/channel/title" />'s Feed</h2>
             <small>Web Feed Preview</small>
            </section>
            <xsl:if test="/rss/channel/atom:link[@rel='next']/@href">
              <icon-button>
               <a>
                 <xsl:attribute name="href">
                   <xsl:value-of select="/rss/channel/atom:link[@rel='next']/@href" />
                 </xsl:attribute>
                 {{ safeHTML (readFile (print (partial "function-paths.html").static "/icons/feather/arrow-right.svg")) }}
                 <small>Next</small>
               </a>
              </icon-button>
            </xsl:if>
            {{ if .Site.Menus.main }}
              {{ range .Site.Menus.main }}
                <icon-navigator hidden="">
                  <icon-button>
                    <a id="nav-middle-{{ path.Base .Identifier }}" href="{{ .URL | relURL }}">
                     {{ with .Identifier }}
                       {{ $icon := print (partial "function-paths.html").static "/icons/" . ".svg" }}
                       {{ safeHTML (readFile $icon) }}
                     {{ end }}
                     <small>{{ delimit (first 1 (split .Name " ")) " " }}</small>
                    </a>
                  </icon-button>
                </icon-navigator>
              {{ end }}
            {{ end }}
          </nav>
          <main>
            <xsl:for-each select="/rss/channel/item">
              <micro-card>
                <header>
                  <figure>
                    <a>
                      <xsl:attribute name="href"><xsl:value-of select="link" /></xsl:attribute>
                      <span>
                        <object>
                          <xsl:attribute name="title"><xsl:value-of select="atom:author/atom:name" /></xsl:attribute>
                          <xsl:attribute name="data"><xsl:value-of select="atom:author/atom:uri" /></xsl:attribute>
                          <xsl:value-of select="substring(atom:author/atom:name, 1, 1)" />
                        </object>
                      </span>
                    </a>
                  </figure>
                </header>
                <article>
                  <h2>
                    <a>
                      <xsl:attribute name="href"><xsl:value-of select="link" /></xsl:attribute>
                      <xsl:value-of select="title" />
                    </a>
                  </h2>
                  <small>Published: <xsl:value-of select="pubDate" /></small>
                </article>
              </micro-card>
            </xsl:for-each>
          </main>
       </column-base>
       <column-base position="right"></column-base>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
