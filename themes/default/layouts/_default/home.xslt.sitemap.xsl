{{- printf `<?xml version="1.0" encoding="utf-8"?>` | safeHTML }}
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:sitemap="http://www.sitemaps.org/schemas/sitemap/0.9"
  version="1.1"
>
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" data-xml="sitemap" data-document="xhtml">
      <head>
        <title>{{ $.Site.Title }} Site Map</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
        {{- partial "head-css.html" . -}}
        {{- partial "head-js.html"  . -}}
      </head>
      <body>
        <column-base position="left"></column-base>
        <column-base position="middle">
          <nav>
            <icon-button>
              <a id="back" target="_self" href="/">
                {{ safeHTML (readFile (print (partial "function-paths.html").static "/icons/feather/arrow-left.svg")) }}
                <small>Back</small>
              </a>
            </icon-button>
            <section>
              <h2>{{ $.Site.Title }} Site Map</h2>
              <small>Site Map Preview</small>
            </section>
            {{ if .Site.Menus.main }}
              {{ range .Site.Menus.main }}
                <icon-navigator hidden="">
                  <icon-button>
                    <a
                      id="nav-middle-{{ path.Base .Identifier }}"
                      href="{{ .URL | relURL }}"
                    >
                      {{ with .Identifier }}
                        {{ $icon := print (partial "function-paths.html").static "/icons/" . ".svg" }}
                        {{ safeHTML (readFile $icon) }}
                      {{ end }}
                      <small>{{ delimit (first 1 (split .Name " ")) " " }}</small>
                    </a>
                  </icon-button>
                </icon-navigator>
              {{ end }}
            {{ end }}
          </nav>
          <main>
            <navigation-separator><hr hidden=""></hr></navigation-separator>
            <section>
              <table>
                <thead>
                  <tr>
                    <th align="left">
                      <code>
                        <xsl:value-of select="count(sitemap:urlset/sitemap:url)"/>
                        Uniform Resource Locators (URLs)
                      </code>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <xsl:for-each select="sitemap:urlset/sitemap:url">
                    <tr>
                      <td>
                        <a>
                          <xsl:attribute name="href">
                            <xsl:value-of select="sitemap:loc" />
                          </xsl:attribute>
<xsl:attribute name="title">Last Modified: <xsl:value-of select="sitemap:lastmod" />
Frequency: <xsl:value-of select="sitemap:changefreq" />
Priority: <xsl:value-of select="sitemap:priority" />
                          </xsl:attribute>
                          <xsl:value-of select="sitemap:loc" />
                        </a>
                      </td>
                    </tr>
                  </xsl:for-each>
                </tbody>
              </table>
            </section>
          </main>
        </column-base>
        <column-base position="right"></column-base>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
