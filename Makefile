help: # Show this help menu
	@printf "\n%s\n\n" "$$(grep -E '^[a-zA-Z0-9 -]+:.*#'  Makefile | column -s ':#' -t)"

watch: # Watch and rebuild everything
	while true; do git ls-files -cdmo --exclude-standard --exclude tests | entr -d -s 'make build'; done

monitor: # Monitor for files changes
	inotifywait --monitor --recursive --event modify .

server: # Start server
	caddy file-server --listen :9294 --root public

build: # Build everything
	make build-config
	make build-js
	make build-site

build-sub: # Build everything into substitute directory
	make build
	mkdir public/m
	mv public/* public/m || true

build-config: # Build configuration files
	yj -yj -i < config/_default/config.yaml > config/_default/config.json
	yj -yt -i < config/_default/config.yaml > config/_default/config.toml

build-js: # Build js bundle
	deno run --allow-all static/js/index-bundle.ts

build-site: # Build site
	rm -rf public
	hugo --logLevel debug --templateMetrics --templateMetricsHints
	hugo --logLevel debug --templateMetrics --templateMetricsHints
	rm -f .hugo_build.lock

build-icons: # Build icons
	rm -rf static/icons
	mkdir -p static/icons/feather static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/LICENSE                           --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/alert-circle.svg            --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/arrow-down-circle.svg       --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/arrow-left.svg              --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/arrow-right.svg             --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/arrow-up.svg                --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/at-sign.svg                 --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/calendar.svg                --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/clock.svg                   --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/edit.svg                    --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/external-link.svg           --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/eye-off.svg                 --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/eye.svg                     --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/file-text.svg               --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/globe.svg                   --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/bookmark.svg                --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/link.svg                    --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/map-pin.svg                 --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/refresh-cw.svg              --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/search.svg                  --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/feathericons/feather/master/icons/trash-2.svg                 --output-dir static/icons/feather
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/LICENSE                            --output-dir static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/icons/outline/archive.svg          --output-dir static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/icons/outline/book-2.svg           --output-dir static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/icons/outline/circle.svg           --output-dir static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/icons/outline/clock.svg            --output-dir static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/icons/outline/code.svg             --output-dir static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/icons/outline/git-fork.svg         --output-dir static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/icons/outline/home.svg             --output-dir static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/icons/outline/notes.svg            --output-dir static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/icons/outline/pinned.svg           --output-dir static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/icons/outline/robot.svg            --output-dir static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/icons/outline/rss.svg              --output-dir static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/icons/outline/settings.svg         --output-dir static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/icons/outline/square-letter-m.svg  --output-dir static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/icons/outline/tag.svg              --output-dir static/icons/tabler
	curl --silent --remote-name https://raw.githubusercontent.com/tabler/tabler-icons/master/icons/outline/users.svg            --output-dir static/icons/tabler
	for icon in static/icons/*/*.svg; do \
		xmlstarlet c14n --without-comments $$icon | xmllint --output $$icon --pretty 2 -; \
		sed --in-place '1d' $$icon; \
	done
	git add -f static/icons

test: # Test everything
	make test-html
	make test-css
	make test-xsl
	make test-xml
	make test-rss
	make test-jsonfeed

test-html: # Test HTML
	vnu \
	--html \
	--errors-only \
	public/index.html \
	public/default/index.html

test-html-indices: # Test HTML indices
	find public/ -type f -name "index.html" -exec vnu --errors-only {} +

test-css: # Test CSS
	vnu \
	--css \
	assets/css/default.css \
	assets/css/default-fast.css \
	assets/css/default-simple.css \
	assets/css/default-syntax.css

test-xsl: # Test XSL
	xsltproc public/rss.xsl
	xsltproc public/sitemap.xsl

test-xml: # Test XML
	xmllint --noout \
	public/rss.xml \
	public/sitemap.xml

test-rss: # Test RSS
	xmllint --noout --relaxng assets/schemas/rss-v2.0.rng public/rss.xml
#	xmllint --noout --schema assets/schemas/rss-v2.0.xsd public/rss.xml

test-rss-indices: # Test RSS indices
	find public -type f -name "rss.xml" ! -path '*/sources/*' -exec xmllint --noout --relaxng assets/schemas/rss-v2.0.rng {} +

test-sitemap: # Test sitemap
	xmllint --noout --schema assets/schemas/sitemap-v0.9.xsd public/sitemap.xml
#	xmllint --noout --schema assets/schemas/sitemapindex-v0.9.xsd public/sitemap.xml

test-jsonfeed: # Test JSONFeed
	find public -type f -name "index.json" ! -path '*/sources/*' -exec check-jsonschema --schemafile assets/schemas/jsonfeed-v1.1.json {} +

test-end: # Test front end
	deno run --allow-all tests/index.ts

test-bed: # Test bed run
	deno run --allow-all tests/test.ts

test-crawl: # Test link parity
	deno run --allow-all tests/crawl.ts

test-crawl-feed: # Test feed link parity
	deno run --allow-all tests/feed.ts

clean: # Clean everything
	make clean-tests

clean-tests: # Clean up tests
	for file in "/tmp/"*;                do { [ -d "$$file/puppeteer_dev_chrome_profile-" ] && rm --recursive --force --verbose "$$file"; } || true; done
	for file in "/run/user/$$(id -u)/"*; do { [ -d "$$file/puppeteer_dev_chrome_profile-" ] && rm --recursive --force --verbose "$$file"; } || true; done
